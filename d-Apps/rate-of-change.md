| Category | Unit of Measurement | 2023 Projection | 2024 Projection | 2025 Projection | 2026 Projection | 2027 Projection |
|----------|-------------------|----------------|----------------|----------------|----------------|----------------|
| Compute  | OPS               | 100-200 billion | 200-400 billion | 400-800 billion | 800-1.6 trillion | 1.6-3.2 trillion |
| Memory   | MB/s              | 20-40 GB/s       | 40-80 GB/s       | 60-120 GB/s      | 80-160 GB/s      | 100-200 GB/s     |
| Latency  | Mbps              | 20-100 Mbps      | 50-200 Mbps      | 80-400 Mbps      | 120-600 Mbps     | 100-1000 Mbps    |

smartphones

| Category | Unit of Measurement | 2023 Projection | 2024 Projection | 2025 Projection | 2026 Projection | 2027 Projection |
|----------|-------------------|----------------|----------------|----------------|----------------|----------------|
| Compute  | OPS               | 100-200 billion | 200-400 billion | 400-800 billion | 800-1.6 trillion | 1.6-3.2 trillion |
| Memory   | MB/s              | 5-10 GB/s        | 10-20 GB/s       | 15-30 GB/s       | 20-40 GB/s       | 25-50 GB/s       |
| Latency  | Mbps              | 20-100 Mbps      | 50-200 Mbps      | 80-400 Mbps      | 120-600 Mbps     | 100-1000 Mbps    |


desktops

| Category | Unit of Measurement | 2023 Projection | 2024 Projection | 2025 Projection | 2026 Projection | 2027 Projection |
|----------|-------------------|----------------|----------------|----------------|----------------|----------------|
| Compute  | OPS               | 1-2 trillion    | 2-4 trillion    | 4-8 trillion    | 8-16 trillion   | 16-32 trillion   |
| Memory   | MB/s              | 100-200 GB/s     | 200-400 GB/s     | 300-600 GB/s     | 400-800 GB/s     | 500-1000 GB/s    |
| Latency  | Mbps              | 1-10 Gbps        | 2-20 Gbps        | 3-30 Gbps        | 4-40 Gbps        | 5-50 Gbps        |
