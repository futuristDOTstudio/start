x
sovereignty of self sos : the case for man in the age of machines



technology and governance have historically enjoyed key privileged base assumptions. advancement in technology is inherently benevolent, evolution of governance by majority rule is indisputably right. these assumptions as a reflection of reality in the current state - and future trajectory - of society, no longer hold true.


this evolving work embodies a first person endeavour towards sovereignty of self. leveraging cryptographically enabled technology, like distributed ledgers, which represent firm foundations for the paradigm shift that enable extrication of these historically intractable assumptions. a technology atop of which first principles exploration of the fundamental nature of language, time and energy - are expressed through the creation of varied artefacts including digital native assets.


ultimately resulting works that challenge by exploring and exploiting technology and governance as levers for liberty. while being an illustration of what is possible at this intersection, this would also in parallel leading to the emergence of a reference implementation for a novel standard of sovereignty.


in essence this is but a continuation of the age old story of man versus machine. this new chapter sees the introduction of accessible participatory constructs for creating and comprehending meaning and purpose - constructs that continue to better inform infrastructure and incentives in favour of the individual.






* * * * * * * * * * * * *

no longer hold true

pseudocode/reproducibility archivibiility
common repository (network/storage/dns),
incentives (security/energy),
sub/obj (compute), 

automation

of the dawn 


techonology is default good - majority rule is default right.
continued development of tools and decision making


along with the historically derived assumption that the best model for consensus/decision making is majority rule. metastasis 

by way of stradlling the disciplines of art and design and by self imposed oscillation between constrained + unconstrained boundaries.

plutocratic accrual of weath and power technologically to fewer and fewer representing central point of failure high possibilty of abuse/corruption conditions conducive 
the illusion of choice  - groupthink leading to detarioration of creativity and capital
the spiral apathy that results from that

automation - man v machine

	Archivability and Reproducibility
	blockchain common source of truth repository
	Incentivised game theory  


.


automation naturally emerges from technological evolution can be ruthlessly efficiently optimises processes to the point - continues to increase over time, exponential rate = runaway reactions…






Categories*
Which of these broad categories best describes your submission:


[ART] NFTs / programmable art / blockchain as a medium

Aesthetics
Programmable art; input/output, code/visual
Blockchain as an artistic medium
Generative art, AI
Seriality, scarcity, rarity, gambling
Valuation (monetary and artistic)
Art vs. collectibles
Anti-NFTs
Historical perspectives on art and technology


[INDUSTRY] production / market / curation / community

Collector’s perspectives
Role of ‘traditional’ galleries and museums
Curatorial models
Online communities
Economics
DAOs
Practicality (short talks about experience)


[IMPACT] intersections between art / technology / society

Utopian vision of blockchain technology / web3 / internet art
Internet and Web3 culture
Inclusivity, equity, democracy
Climate change and energy
Technology (Software, design, marketplaces, possibilities and limitations)
Digital assets and society


Call for Papers
We are currently seeking submissions for proposed talks from any of the aforementioned fields. We will accept submissions from anyone with or without an academic/institutional affiliation as long as academic standards are met, as the aim of the conference is to bring together researchers and industry professionals. We welcome critical perspectives on blockchain technology and NFTs.

Proposals for presentations must be submitted by the 15th of July, and should consist of a title and abstract of no more than 250 words. Speakers may present about their academic research, professional experience, or an artistic project. All abstracts will be subjected to anonymous peer review. If accepted, authors may choose to present in-person or virtually. Presentations may take the form of a short talk (15 minutes), or a poster. Some speakers may be allocated to roundtable discussions to facilitate interaction between speakers.


* * * * * * * * * * * * *

handwriting.signature/identity_encoded_and_encrypted
objective, method, result, conclusion

< body>

since time immemorial humans have sort to find better ways to create means of communication and identity while retain the integrity of their intent. from name seals to handwritten wet signatures, yet a rigorous robust implementation continued to allude us until the advent of modern computers and cryptography. it is now within the realm of possibility for the individual to protect their identity and intents from formidable adversaries.

the objective of this work is to explore encoding and encrypting my identity and intents -  on-line and off-line, on-chain and off-chain. exploringe the evolving nature of identity in language and its objective and subjective states.

building upon the owned decentralised .signature (and .handwriting) top level domain - creative uses of the entire domain name space from/including root.

< /body>

sld tld


resulting in reference implementation from artistic installation/intervention to decentralised civic utility tool that could denote familial ties or other diverse affinity oriented identity contexts acting as unique inputs.


history of communication, integrity, centrality to civilisation, culture and commerce - codes and ciphers - formidable adversary, david versus goliath

human markmaking, traces, voice, identity - adaptable. evidence of existance of an individual to an entire people

robust reproducibility and archivability


ultimately illustrating cryptographically verifiable native assets are more than a link - , energy toward protecting identity value in the creative unique existance of the individual
from art to utility, zero to one, a to n

fingerprinting - surveillance state economy, liberate the individuals identity from being state owned through corporations 





* * * * * * * * * * * * *

d-app.store - a new dimension of applications
d-app.store - unlocking a whole new dimension of applications
objective, method, result, conclusion

the web has evolved. from web 1 in its nascent stages where it was predominantly read only on desktops - to web 2 where it became read and write propelled by the transition to mobile. web 3 however is experiencing a stagnation owing to legacy browsers and businesses being preoccupied with their existing web 2 features and advertising private data mining business model.


for the web 3 paradigm shift, embracing its novel affordances during this early state change would increase the probabilities of it an optimal outcome in decentralisation ideals. a sandbox for the creative and for the curious early adopters pioneering the frontier. having an unencumbered experience of what's possible - a browser that is crypto native, 3D native, decentralised infrastructure native. 


this could be accomplished by implementing nascent technologies which together as a whole would greater than the sum of its parts. decentralised networking though libp2p, decentralised dns through handshake, eutxo native assets and scripts but to name a few would not only enable unadulterated connectivity to domains but also decentralised communication through email mx records - enabling networks upon networks of communities. this would not only elevate the onboarding custody experience, but also go a long way to catalyse these nascent technologies whilst in parallel creating a common and compounding audience to cross pollinate - achieving substantial network effects.





stack - network layer, storage layer, compute layer

armature - 3D foundations, gltf








curation - custody
canvas
exploration - , 





















